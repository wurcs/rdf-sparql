# WURCS-RDF and SPARQL Query

## WURCS to WURCS-RDF

* https://glyconavi.org/Tools/tool/cosmos.php using https://api.glycosmos.org/

* 2021-11-10 **N-linked GlcNAc2Man3 core**

  * https://glyconavi.org/Tools/tool/cosmos.php -> WURCS TO WURCS-RDF

  `WURCS=2.0/3,5,4/[a2122h-1b_1-5_2*NCC/3=O][a1122h-1b_1-5][a1122h-1a_1-5]/1-1-2-3-3/a4-b1_b4-c1_c3-d1_c6-e1`
   
   ![G22768VO](G22768VO.png)
   
   https://glytoucan.org/Structures/Glycans/G22768VO

### WURCS-RDF of `WURCS=2.0/3,5,4/[a2122h-1b_1-5_2*NCC/3=O][a1122h-1b_1-5][a1122h-1a_1-5]/1-1-2-3-3/a4-b1_b4-c1_c3-d1_c6-e1`

```
<http://rdf.glycoinfo.org/glycan/wurcs/2.0/GLIP/c3>
        a       <http://www.glycoinfo.org/glyco/owl/wurcs#GLIP> ;
        <http://www.glycoinfo.org/glyco/owl/wurcs#has_RES>
                <http://rdf.glycoinfo.org/glycan/wurcs/2.0/RES/c> ;
        <http://www.glycoinfo.org/glyco/owl/wurcs#has_SC_position>
                3 .

<http://rdf.glycoinfo.org/glycan/wurcs/2.0/GLIPS/b1>
        a       <http://www.glycoinfo.org/glyco/owl/wurcs#GLIPS> ;
        <http://www.glycoinfo.org/glyco/owl/wurcs#has_GLIP>
                <http://rdf.glycoinfo.org/glycan/wurcs/2.0/GLIP/b1> ;
        <http://www.glycoinfo.org/glyco/owl/wurcs#is_fuzzy>
                false .

<http://rdf.glycoinfo.org/glycan/G000000OO/wurcs/2.0/RES/c>
        a       <http://www.glycoinfo.org/glyco/owl/wurcs#RES> ;
        <http://www.glycoinfo.org/glyco/owl/wurcs#has_LIN>
                <http://rdf.glycoinfo.org/glycan/G000000OO/wurcs/2.0/LIN/c6-e1> , <http://rdf.glycoinfo.org/glycan/G000000OO/wurcs/2.0/LIN/c3-d1> , <http://rdf.glycoinfo.org/glycan/G000000OO/wurcs/2.0/LIN/b4-c1> ;
        <http://www.glycoinfo.org/glyco/owl/wurcs#is_uniqueRES>
                <http://rdf.glycoinfo.org/glycan/G000000OO/wurcs/2.0/UniqueRES/2> .

<http://rdf.glycoinfo.org/glycan/wurcs/2.0/GLIP/a4>
        a       <http://www.glycoinfo.org/glyco/owl/wurcs#GLIP> ;
        <http://www.glycoinfo.org/glyco/owl/wurcs#has_RES>
                <http://rdf.glycoinfo.org/glycan/wurcs/2.0/RES/a> ;
        <http://www.glycoinfo.org/glyco/owl/wurcs#has_SC_position>
                4 .

<http://rdf.glycoinfo.org/glycan/wurcs/2.0/GLIPS/c6>
        a       <http://www.glycoinfo.org/glyco/owl/wurcs#GLIPS> ;
        <http://www.glycoinfo.org/glyco/owl/wurcs#has_GLIP>
                <http://rdf.glycoinfo.org/glycan/wurcs/2.0/GLIP/c6> ;
        <http://www.glycoinfo.org/glyco/owl/wurcs#is_fuzzy>
                false .

<http://rdf.glycoinfo.org/glycan/wurcs/2.0/GLIP/e1>
        a       <http://www.glycoinfo.org/glyco/owl/wurcs#GLIP> ;
        <http://www.glycoinfo.org/glyco/owl/wurcs#has_RES>
                <http://rdf.glycoinfo.org/glycan/wurcs/2.0/RES/e> ;
        <http://www.glycoinfo.org/glyco/owl/wurcs#has_SC_position>
                1 .

<http://rdf.glycoinfo.org/glycan/G000000OO/wurcs/2.0/UniqueRES/3>
        a       <http://www.glycoinfo.org/glyco/owl/wurcs#UniqueRES> ;
        <http://www.glycoinfo.org/glyco/owl/wurcs#is_monosaccharide>
                <http://rdf.glycoinfo.org/glycan/wurcs/2.0/Monosaccharide/a1122h-1a_1-5> .

<http://rdf.glycoinfo.org/glycan/G000000OO>
        a       <http://purl.jp/bio/12/glyco/glycan#saccharide> ;
        <http://purl.jp/bio/12/glyco/glycan#has_glycosequence>
                <http://rdf.glycoinfo.org/glycan/G000000OO/wurcs/2.0> .

<http://rdf.glycoinfo.org/glycan/G000000OO/wurcs/2.0/RES/b>
        a       <http://www.glycoinfo.org/glyco/owl/wurcs#RES> ;
        <http://www.glycoinfo.org/glyco/owl/wurcs#has_LIN>
                <http://rdf.glycoinfo.org/glycan/G000000OO/wurcs/2.0/LIN/b4-c1> , <http://rdf.glycoinfo.org/glycan/G000000OO/wurcs/2.0/LIN/a4-b1> ;
        <http://www.glycoinfo.org/glyco/owl/wurcs#is_uniqueRES>
                <http://rdf.glycoinfo.org/glycan/G000000OO/wurcs/2.0/UniqueRES/1> .

<http://rdf.glycoinfo.org/glycan/wurcs/2.0/GLIP/d1>
        a       <http://www.glycoinfo.org/glyco/owl/wurcs#GLIP> ;
        <http://www.glycoinfo.org/glyco/owl/wurcs#has_RES>
                <http://rdf.glycoinfo.org/glycan/wurcs/2.0/RES/d> ;
        <http://www.glycoinfo.org/glyco/owl/wurcs#has_SC_position>
                1 .

<http://rdf.glycoinfo.org/glycan/G000000OO/wurcs/2.0/LIN/c6-e1>
        a       <http://www.glycoinfo.org/glyco/owl/wurcs#LIN> ;
        <http://www.glycoinfo.org/glyco/owl/wurcs#has_GLIPS>
                <http://rdf.glycoinfo.org/glycan/wurcs/2.0/GLIPS/e1> , <http://rdf.glycoinfo.org/glycan/wurcs/2.0/GLIPS/c6> ;
        <http://www.glycoinfo.org/glyco/owl/wurcs#is_repeat>
                false .

<http://rdf.glycoinfo.org/glycan/wurcs/2.0/GLIP/c1>
        a       <http://www.glycoinfo.org/glyco/owl/wurcs#GLIP> ;
        <http://www.glycoinfo.org/glyco/owl/wurcs#has_RES>
                <http://rdf.glycoinfo.org/glycan/wurcs/2.0/RES/c> ;
        <http://www.glycoinfo.org/glyco/owl/wurcs#has_SC_position>
                1 .

<http://rdf.glycoinfo.org/glycan/G000000OO/wurcs/2.0/LIN/c3-d1>
        a       <http://www.glycoinfo.org/glyco/owl/wurcs#LIN> ;
        <http://www.glycoinfo.org/glyco/owl/wurcs#has_GLIPS>
                <http://rdf.glycoinfo.org/glycan/wurcs/2.0/GLIPS/d1> , <http://rdf.glycoinfo.org/glycan/wurcs/2.0/GLIPS/c3> ;
        <http://www.glycoinfo.org/glyco/owl/wurcs#is_repeat>
                false .

<http://rdf.glycoinfo.org/glycan/G000000OO/wurcs/2.0/UniqueRES/2>
        a       <http://www.glycoinfo.org/glyco/owl/wurcs#UniqueRES> ;
        <http://www.glycoinfo.org/glyco/owl/wurcs#is_monosaccharide>
                <http://rdf.glycoinfo.org/glycan/wurcs/2.0/Monosaccharide/a1122h-1b_1-5> .

<http://rdf.glycoinfo.org/glycan/G000000OO/wurcs/2.0>
        a       <http://purl.jp/bio/12/glyco/glycan#glycosequence> ;
        <http://purl.jp/bio/12/glyco/glycan#has_sequence>
                "WURCS=2.0/3,5,4/[a2122h-1b_1-5_2*NCC/3=O][a1122h-1b_1-5][a1122h-1a_1-5]/1-1-2-3-3/a4-b1_b4-c1_c3-d1_c6-e1"^^<http://www.w3.org/2001/XMLSchema#string> ;
        <http://purl.jp/bio/12/glyco/glycan#in_carbohydrate_format>
                <http://purl.jp/bio/12/glyco/glycan#carbohydrate_format_wurcs> ;
        <http://www.glycoinfo.org/glyco/owl/wurcs#LIN_count>
                4 ;
        <http://www.glycoinfo.org/glyco/owl/wurcs#RES_count>
                5 ;
        <http://www.glycoinfo.org/glyco/owl/wurcs#has_LIN>
                <http://rdf.glycoinfo.org/glycan/G000000OO/wurcs/2.0/LIN/b4-c1> , <http://rdf.glycoinfo.org/glycan/G000000OO/wurcs/2.0/LIN/c6-e1> , <http://rdf.glycoinfo.org/glycan/G000000OO/wurcs/2.0/LIN/a4-b1> , <http://rdf.glycoinfo.org/glycan/G000000OO/wurcs/2.0/LIN/c3-d1> ;
        <http://www.glycoinfo.org/glyco/owl/wurcs#has_basetype>
                <http://rdf.glycoinfo.org/glycan/wurcs/2.0/Basetype/u1122h> , <http://rdf.glycoinfo.org/glycan/wurcs/2.0/Basetype/u2122h_2%2ANCC%2F3%3DO> ;
        <http://www.glycoinfo.org/glyco/owl/wurcs#has_monosaccharide>
                <http://rdf.glycoinfo.org/glycan/wurcs/2.0/Monosaccharide/a2122h-1b_1-5_2%2ANCC%2F3%3DO> , <http://rdf.glycoinfo.org/glycan/wurcs/2.0/Monosaccharide/a1122h-1b_1-5> , <http://rdf.glycoinfo.org/glycan/wurcs/2.0/Monosaccharide/a1122h-1a_1-5> ;
        <http://www.glycoinfo.org/glyco/owl/wurcs#has_root_RES>
                <http://rdf.glycoinfo.org/glycan/G000000OO/wurcs/2.0/RES/a> ;
        <http://www.glycoinfo.org/glyco/owl/wurcs#has_uniqueRES>
                <http://rdf.glycoinfo.org/glycan/G000000OO/wurcs/2.0/UniqueRES/1> , <http://rdf.glycoinfo.org/glycan/G000000OO/wurcs/2.0/UniqueRES/2> , <http://rdf.glycoinfo.org/glycan/G000000OO/wurcs/2.0/UniqueRES/3> ;
        <http://www.glycoinfo.org/glyco/owl/wurcs#uniqueRES_count>
                3 ;
        <http://www.w3.org/2002/07/owl#sameAs>
                <http://rdf.glycoinfo.org/glycan/wurcs/2.0/WURCS%3D2.0%2F3%2C5%2C4%2F%5Ba2122h-1b_1-5_2%2ANCC%2F3%3DO%5D%5Ba1122h-1b_1-5%5D%5Ba1122h-1a_1-5%5D%2F1-1-2-3-3%2Fa4-b1_b4-c1_c3-d1_c6-e1> .

<http://rdf.glycoinfo.org/glycan/G000000OO/wurcs/2.0/RES/a>
        a       <http://www.glycoinfo.org/glyco/owl/wurcs#RES> ;
        <http://www.glycoinfo.org/glyco/owl/wurcs#has_LIN>
                <http://rdf.glycoinfo.org/glycan/G000000OO/wurcs/2.0/LIN/a4-b1> ;
        <http://www.glycoinfo.org/glyco/owl/wurcs#is_uniqueRES>
                <http://rdf.glycoinfo.org/glycan/G000000OO/wurcs/2.0/UniqueRES/1> .

<http://rdf.glycoinfo.org/glycan/wurcs/2.0/GLIP/b1>
        a       <http://www.glycoinfo.org/glyco/owl/wurcs#GLIP> ;
        <http://www.glycoinfo.org/glyco/owl/wurcs#has_RES>
                <http://rdf.glycoinfo.org/glycan/wurcs/2.0/RES/b> ;
        <http://www.glycoinfo.org/glyco/owl/wurcs#has_SC_position>
                1 .

<http://rdf.glycoinfo.org/glycan/G000000OO/wurcs/2.0/UniqueRES/1>
        a       <http://www.glycoinfo.org/glyco/owl/wurcs#UniqueRES> ;
        <http://www.glycoinfo.org/glyco/owl/wurcs#is_monosaccharide>
                <http://rdf.glycoinfo.org/glycan/wurcs/2.0/Monosaccharide/a2122h-1b_1-5_2%2ANCC%2F3%3DO> .

<http://rdf.glycoinfo.org/glycan/wurcs/2.0/GLIP/c6>
        a       <http://www.glycoinfo.org/glyco/owl/wurcs#GLIP> ;
        <http://www.glycoinfo.org/glyco/owl/wurcs#has_RES>
                <http://rdf.glycoinfo.org/glycan/wurcs/2.0/RES/c> ;
        <http://www.glycoinfo.org/glyco/owl/wurcs#has_SC_position>
                6 .

<http://rdf.glycoinfo.org/glycan/wurcs/2.0/GLIPS/b4>
        a       <http://www.glycoinfo.org/glyco/owl/wurcs#GLIPS> ;
        <http://www.glycoinfo.org/glyco/owl/wurcs#has_GLIP>
                <http://rdf.glycoinfo.org/glycan/wurcs/2.0/GLIP/b4> ;
        <http://www.glycoinfo.org/glyco/owl/wurcs#is_fuzzy>
                false .

<http://rdf.glycoinfo.org/glycan/G000000OO/wurcs/2.0/LIN/b4-c1>
        a       <http://www.glycoinfo.org/glyco/owl/wurcs#LIN> ;
        <http://www.glycoinfo.org/glyco/owl/wurcs#has_GLIPS>
                <http://rdf.glycoinfo.org/glycan/wurcs/2.0/GLIPS/c1> , <http://rdf.glycoinfo.org/glycan/wurcs/2.0/GLIPS/b4> ;
        <http://www.glycoinfo.org/glyco/owl/wurcs#is_repeat>
                false .

<http://rdf.glycoinfo.org/glycan/wurcs/2.0/GLIPS/c3>
        a       <http://www.glycoinfo.org/glyco/owl/wurcs#GLIPS> ;
        <http://www.glycoinfo.org/glyco/owl/wurcs#has_GLIP>
                <http://rdf.glycoinfo.org/glycan/wurcs/2.0/GLIP/c3> ;
        <http://www.glycoinfo.org/glyco/owl/wurcs#is_fuzzy>
                false .

<http://rdf.glycoinfo.org/glycan/wurcs/2.0/GLIPS/a4>
        a       <http://www.glycoinfo.org/glyco/owl/wurcs#GLIPS> ;
        <http://www.glycoinfo.org/glyco/owl/wurcs#has_GLIP>
                <http://rdf.glycoinfo.org/glycan/wurcs/2.0/GLIP/a4> ;
        <http://www.glycoinfo.org/glyco/owl/wurcs#is_fuzzy>
                false .

<http://rdf.glycoinfo.org/glycan/G000000OO/wurcs/2.0/RES/e>
        a       <http://www.glycoinfo.org/glyco/owl/wurcs#RES> ;
        <http://www.glycoinfo.org/glyco/owl/wurcs#has_LIN>
                <http://rdf.glycoinfo.org/glycan/G000000OO/wurcs/2.0/LIN/c6-e1> ;
        <http://www.glycoinfo.org/glyco/owl/wurcs#is_uniqueRES>
                <http://rdf.glycoinfo.org/glycan/G000000OO/wurcs/2.0/UniqueRES/3> .

<http://rdf.glycoinfo.org/glycan/wurcs/2.0/GLIPS/e1>
        a       <http://www.glycoinfo.org/glyco/owl/wurcs#GLIPS> ;
        <http://www.glycoinfo.org/glyco/owl/wurcs#has_GLIP>
                <http://rdf.glycoinfo.org/glycan/wurcs/2.0/GLIP/e1> ;
        <http://www.glycoinfo.org/glyco/owl/wurcs#is_fuzzy>
                false .

<http://rdf.glycoinfo.org/glycan/G000000OO/wurcs/2.0/LIN/a4-b1>
        a       <http://www.glycoinfo.org/glyco/owl/wurcs#LIN> ;
        <http://www.glycoinfo.org/glyco/owl/wurcs#has_GLIPS>
                <http://rdf.glycoinfo.org/glycan/wurcs/2.0/GLIPS/b1> , <http://rdf.glycoinfo.org/glycan/wurcs/2.0/GLIPS/a4> ;
        <http://www.glycoinfo.org/glyco/owl/wurcs#is_repeat>
                false .

<http://rdf.glycoinfo.org/glycan/wurcs/2.0/GLIPS/d1>
        a       <http://www.glycoinfo.org/glyco/owl/wurcs#GLIPS> ;
        <http://www.glycoinfo.org/glyco/owl/wurcs#has_GLIP>
                <http://rdf.glycoinfo.org/glycan/wurcs/2.0/GLIP/d1> ;
        <http://www.glycoinfo.org/glyco/owl/wurcs#is_fuzzy>
                false .

<http://rdf.glycoinfo.org/glycan/G000000OO/wurcs/2.0/RES/d>
        a       <http://www.glycoinfo.org/glyco/owl/wurcs#RES> ;
        <http://www.glycoinfo.org/glyco/owl/wurcs#has_LIN>
                <http://rdf.glycoinfo.org/glycan/G000000OO/wurcs/2.0/LIN/c3-d1> ;
        <http://www.glycoinfo.org/glyco/owl/wurcs#is_uniqueRES>
                <http://rdf.glycoinfo.org/glycan/G000000OO/wurcs/2.0/UniqueRES/3> .

<http://rdf.glycoinfo.org/glycan/wurcs/2.0/GLIPS/c1>
        a       <http://www.glycoinfo.org/glyco/owl/wurcs#GLIPS> ;
        <http://www.glycoinfo.org/glyco/owl/wurcs#has_GLIP>
                <http://rdf.glycoinfo.org/glycan/wurcs/2.0/GLIP/c1> ;
        <http://www.glycoinfo.org/glyco/owl/wurcs#is_fuzzy>
                false .

<http://rdf.glycoinfo.org/glycan/wurcs/2.0/GLIP/b4>
        a       <http://www.glycoinfo.org/glyco/owl/wurcs#GLIP> ;
        <http://www.glycoinfo.org/glyco/owl/wurcs#has_RES>
                <http://rdf.glycoinfo.org/glycan/wurcs/2.0/RES/b> ;
        <http://www.glycoinfo.org/glyco/owl/wurcs#has_SC_position>
                4 .
<http://rdf.glycoinfo.org/glycan/G000000OO/wurcs/2.0/GRES/4>
        <http://www.glycoinfo.org/glyco/owl/wurcs#is_donor_of>
                <http://rdf.glycoinfo.org/glycan/G000000OO/wurcs/2.0/GLIN/3> ;
        <http://www.glycoinfo.org/glyco/owl/wurcs#is_monosaccharide>
                <http://rdf.glycoinfo.org/glycan/wurcs/2.0/Monosaccharide/a1122h-1a_1-5> .

<http://rdf.glycoinfo.org/glycan/G000000OO/wurcs/2.0/GLIN/1>
        <http://www.glycoinfo.org/glyco/owl/wurcs#has_MAP>
                ""^^<http://www.w3.org/2001/XMLSchema#string> ;
        <http://www.glycoinfo.org/glyco/owl/wurcs#has_acceptor>
                <http://rdf.glycoinfo.org/glycan/G000000OO/wurcs/2.0/GRES/1> ;
        <http://www.glycoinfo.org/glyco/owl/wurcs#has_acceptor_position>
                4 ;
        <http://www.glycoinfo.org/glyco/owl/wurcs#has_donor>
                <http://rdf.glycoinfo.org/glycan/G000000OO/wurcs/2.0/GRES/2> ;
        <http://www.glycoinfo.org/glyco/owl/wurcs#has_donor_position>
                1 .

<http://rdf.glycoinfo.org/glycan/G000000OO/wurcs/2.0/GRES/2>
        <http://www.glycoinfo.org/glyco/owl/wurcs#is_acceptor_of>
                <http://rdf.glycoinfo.org/glycan/G000000OO/wurcs/2.0/GLIN/2> ;
        <http://www.glycoinfo.org/glyco/owl/wurcs#is_donor_of>
                <http://rdf.glycoinfo.org/glycan/G000000OO/wurcs/2.0/GLIN/1> ;
        <http://www.glycoinfo.org/glyco/owl/wurcs#is_monosaccharide>
                <http://rdf.glycoinfo.org/glycan/wurcs/2.0/Monosaccharide/a2122h-1b_1-5_2%2ANCC%2F3%3DO> .

<http://rdf.glycoinfo.org/glycan/G000000OO/wurcs/2.0/GLIN/4>
        <http://www.glycoinfo.org/glyco/owl/wurcs#has_MAP>
                ""^^<http://www.w3.org/2001/XMLSchema#string> ;
        <http://www.glycoinfo.org/glyco/owl/wurcs#has_acceptor>
                <http://rdf.glycoinfo.org/glycan/G000000OO/wurcs/2.0/GRES/3> ;
        <http://www.glycoinfo.org/glyco/owl/wurcs#has_acceptor_position>
                6 ;
        <http://www.glycoinfo.org/glyco/owl/wurcs#has_donor>
                <http://rdf.glycoinfo.org/glycan/G000000OO/wurcs/2.0/GRES/5> ;
        <http://www.glycoinfo.org/glyco/owl/wurcs#has_donor_position>
                1 .

<http://rdf.glycoinfo.org/glycan/G000000OO>
        <http://purl.jp/bio/12/glyco/glycan#has_glycosequence>
                <http://rdf.glycoinfo.org/glycan/G000000OO/wurcs/2.0> .

<http://rdf.glycoinfo.org/glycan/G000000OO/wurcs/2.0/GRES/5>
        <http://www.glycoinfo.org/glyco/owl/wurcs#is_donor_of>
                <http://rdf.glycoinfo.org/glycan/G000000OO/wurcs/2.0/GLIN/4> ;
        <http://www.glycoinfo.org/glyco/owl/wurcs#is_monosaccharide>
                <http://rdf.glycoinfo.org/glycan/wurcs/2.0/Monosaccharide/a1122h-1a_1-5> .

<http://rdf.glycoinfo.org/glycan/G000000OO/wurcs/2.0/GLIN/2>
        <http://www.glycoinfo.org/glyco/owl/wurcs#has_MAP>
                ""^^<http://www.w3.org/2001/XMLSchema#string> ;
        <http://www.glycoinfo.org/glyco/owl/wurcs#has_acceptor>
                <http://rdf.glycoinfo.org/glycan/G000000OO/wurcs/2.0/GRES/2> ;
        <http://www.glycoinfo.org/glyco/owl/wurcs#has_acceptor_position>
                4 ;
        <http://www.glycoinfo.org/glyco/owl/wurcs#has_donor>
                <http://rdf.glycoinfo.org/glycan/G000000OO/wurcs/2.0/GRES/3> ;
        <http://www.glycoinfo.org/glyco/owl/wurcs#has_donor_position>
                1 .

<http://rdf.glycoinfo.org/glycan/G000000OO/wurcs/2.0/GRES/3>
        <http://www.glycoinfo.org/glyco/owl/wurcs#is_acceptor_of>
                <http://rdf.glycoinfo.org/glycan/G000000OO/wurcs/2.0/GLIN/4> , <http://rdf.glycoinfo.org/glycan/G000000OO/wurcs/2.0/GLIN/3> ;
        <http://www.glycoinfo.org/glyco/owl/wurcs#is_donor_of>
                <http://rdf.glycoinfo.org/glycan/G000000OO/wurcs/2.0/GLIN/2> ;
        <http://www.glycoinfo.org/glyco/owl/wurcs#is_monosaccharide>
                <http://rdf.glycoinfo.org/glycan/wurcs/2.0/Monosaccharide/a1122h-1b_1-5> .

<http://rdf.glycoinfo.org/glycan/G000000OO/wurcs/2.0>
        <http://purl.jp/bio/12/glyco/glycan#has_sequence>
                "WURCS=2.0/3,5,4/[a2122h-1b_1-5_2*NCC/3=O][a1122h-1b_1-5][a1122h-1a_1-5]/1-1-2-3-3/a4-b1_b4-c1_c3-d1_c6-e1"^^<http://www.w3.org/2001/XMLSchema#string> ;
        <http://www.glycoinfo.org/glyco/owl/wurcs#LIN_count>
                4 ;
        <http://www.glycoinfo.org/glyco/owl/wurcs#RES_count>
                5 ;
        <http://www.glycoinfo.org/glyco/owl/wurcs#count_LIN>
                4 ;
        <http://www.glycoinfo.org/glyco/owl/wurcs#count_RES>
                5 ;
        <http://www.glycoinfo.org/glyco/owl/wurcs#count_uniqueRES>
                3 ;
        <http://www.glycoinfo.org/glyco/owl/wurcs#has_GRES>
                <http://rdf.glycoinfo.org/glycan/G000000OO/wurcs/2.0/GRES/1> , <http://rdf.glycoinfo.org/glycan/G000000OO/wurcs/2.0/GRES/4> , <http://rdf.glycoinfo.org/glycan/G000000OO/wurcs/2.0/GRES/5> , <http://rdf.glycoinfo.org/glycan/G000000OO/wurcs/2.0/GRES/3> , <http://rdf.glycoinfo.org/glycan/G000000OO/wurcs/2.0/GRES/2> ;
        <http://www.glycoinfo.org/glyco/owl/wurcs#has_root_GRES>
                <http://rdf.glycoinfo.org/glycan/G000000OO/wurcs/2.0/GRES/1> ;
        <http://www.glycoinfo.org/glyco/owl/wurcs#uniqueRES_count>
                3 .

<http://rdf.glycoinfo.org/glycan/G000000OO/wurcs/2.0/GRES/1>
        <http://www.glycoinfo.org/glyco/owl/wurcs#is_acceptor_of>
                <http://rdf.glycoinfo.org/glycan/G000000OO/wurcs/2.0/GLIN/1> ;
        <http://www.glycoinfo.org/glyco/owl/wurcs#is_monosaccharide>
                <http://rdf.glycoinfo.org/glycan/wurcs/2.0/Monosaccharide/a2122h-1b_1-5_2%2ANCC%2F3%3DO> .

<http://rdf.glycoinfo.org/glycan/G000000OO/wurcs/2.0/GLIN/3>
        <http://www.glycoinfo.org/glyco/owl/wurcs#has_MAP>
                ""^^<http://www.w3.org/2001/XMLSchema#string> ;
        <http://www.glycoinfo.org/glyco/owl/wurcs#has_acceptor>
                <http://rdf.glycoinfo.org/glycan/G000000OO/wurcs/2.0/GRES/3> ;
        <http://www.glycoinfo.org/glyco/owl/wurcs#has_acceptor_position>
                3 ;
        <http://www.glycoinfo.org/glyco/owl/wurcs#has_donor>
                <http://rdf.glycoinfo.org/glycan/G000000OO/wurcs/2.0/GRES/4> ;
        <http://www.glycoinfo.org/glyco/owl/wurcs#has_donor_position>
                1 .
<http://rdf.glycoinfo.org/glycan/wurcs/2.0/LIP/5>
        a       <http://www.glycoinfo.org/glyco/owl/wurcs#LIP> ;
        <http://www.glycoinfo.org/glyco/owl/wurcs#has_SC_position>
                5 .

<http://rdf.glycoinfo.org/glycan/wurcs/2.0/Monosaccharide/axxxxh-1b_1-%3F_2%2ANCC%2F3%3DO>
        a       <http://www.glycoinfo.org/glyco/owl/wurcs#Monosaccharide> ;
        <http://www.glycoinfo.org/glyco/owl/wurcs#subsumes>
                <http://rdf.glycoinfo.org/glycan/wurcs/2.0/Monosaccharide/a2122h-1b_1-5_2%2ANCC%2F3%3DO> .

<http://rdf.glycoinfo.org/glycan/wurcs/2.0/Monosaccharide/axxxxh-1a_1-%3F>
        a       <http://www.glycoinfo.org/glyco/owl/wurcs#Monosaccharide> ;
        <http://www.glycoinfo.org/glyco/owl/wurcs#subsumes>
                <http://rdf.glycoinfo.org/glycan/wurcs/2.0/Monosaccharide/a1122h-1a_1-5> .

<http://rdf.glycoinfo.org/glycan/wurcs/2.0/Monosaccharide/u2122h_2%2ANCC%2F3%3DO>
        a       <http://www.glycoinfo.org/glyco/owl/wurcs#Monosaccharide> ;
        <http://www.glycoinfo.org/glyco/owl/wurcs#subsumes>
                <http://rdf.glycoinfo.org/glycan/wurcs/2.0/Monosaccharide/a2122h-1b_1-5_2%2ANCC%2F3%3DO> .

<http://rdf.glycoinfo.org/glycan/wurcs/2.0/MOD/1-5>
        a       <http://www.glycoinfo.org/glyco/owl/wurcs#MOD> ;
        <http://www.glycoinfo.org/glyco/owl/wurcs#has_LIPS>
                <http://rdf.glycoinfo.org/glycan/wurcs/2.0/LIPS/5> , <http://rdf.glycoinfo.org/glycan/wurcs/2.0/LIPS/1> .

<http://rdf.glycoinfo.org/glycan/wurcs/2.0/Monosaccharide/a2122h-1b_1-5_2%2ANCC%2F3%3DO>
        a       <http://www.glycoinfo.org/glyco/owl/wurcs#Monosaccharide> ;
        <http://www.glycoinfo.org/glyco/owl/wurcs#has_MOD>
                <http://rdf.glycoinfo.org/glycan/wurcs/2.0/MOD/2%2ANCC%2F3%3DO> , <http://rdf.glycoinfo.org/glycan/wurcs/2.0/MOD/1-5> ;
        <http://www.glycoinfo.org/glyco/owl/wurcs#has_SC>
                <http://rdf.glycoinfo.org/glycan/wurcs/2.0/SkeletonCode/a2122h> ;
        <http://www.glycoinfo.org/glyco/owl/wurcs#has_anomeric_position>
                1 ;
        <http://www.glycoinfo.org/glyco/owl/wurcs#has_anomeric_symbol>
                "b"^^<http://www.w3.org/2001/XMLSchema#string> ;
        <http://www.glycoinfo.org/glyco/owl/wurcs#has_basetype>
                <http://rdf.glycoinfo.org/glycan/wurcs/2.0/Basetype/u2122h_2%2ANCC%2F3%3DO> ;
        <http://www.glycoinfo.org/glyco/owl/wurcs#has_ring>
                <http://rdf.glycoinfo.org/glycan/wurcs/2.0/MOD/1-5> ;
        <http://www.glycoinfo.org/glyco/owl/wurcs#subsumes>
                <http://rdf.glycoinfo.org/glycan/wurcs/2.0/Monosaccharide/a2122h-1b_1-5_2%2ANCC%2F3%3DO> .

<http://rdf.glycoinfo.org/glycan/wurcs/2.0/Monosaccharide/a3344h-1a_1-5>
        a       <http://www.glycoinfo.org/glyco/owl/wurcs#Monosaccharide> ;
        <http://www.glycoinfo.org/glyco/owl/wurcs#subsumes>
                <http://rdf.glycoinfo.org/glycan/wurcs/2.0/Monosaccharide/a1122h-1a_1-5> .

<http://rdf.glycoinfo.org/glycan/wurcs/2.0/LIPS/5>
        a       <http://www.glycoinfo.org/glyco/owl/wurcs#LIPS> ;
        <http://www.glycoinfo.org/glyco/owl/wurcs#has_LIP>
                <http://rdf.glycoinfo.org/glycan/wurcs/2.0/LIP/5> ;
        <http://www.glycoinfo.org/glyco/owl/wurcs#is_fuzzy>
                false .

<http://rdf.glycoinfo.org/glycan/wurcs/2.0/Monosaccharide/a3344h-1x_1-%3F>
        a       <http://www.glycoinfo.org/glyco/owl/wurcs#Monosaccharide> ;
        <http://www.glycoinfo.org/glyco/owl/wurcs#subsumes>
                <http://rdf.glycoinfo.org/glycan/wurcs/2.0/Monosaccharide/a1122h-1b_1-5> , <http://rdf.glycoinfo.org/glycan/wurcs/2.0/Monosaccharide/a1122h-1a_1-5> .

<http://rdf.glycoinfo.org/glycan/wurcs/2.0/Monosaccharide/a3344h-1x_1-5>
        a       <http://www.glycoinfo.org/glyco/owl/wurcs#Monosaccharide> ;
        <http://www.glycoinfo.org/glyco/owl/wurcs#subsumes>
                <http://rdf.glycoinfo.org/glycan/wurcs/2.0/Monosaccharide/a1122h-1b_1-5> , <http://rdf.glycoinfo.org/glycan/wurcs/2.0/Monosaccharide/a1122h-1a_1-5> .

<http://rdf.glycoinfo.org/glycan/wurcs/2.0/Monosaccharide/a3344h-1b_1-5>
        a       <http://www.glycoinfo.org/glyco/owl/wurcs#Monosaccharide> ;
        <http://www.glycoinfo.org/glyco/owl/wurcs#subsumes>
                <http://rdf.glycoinfo.org/glycan/wurcs/2.0/Monosaccharide/a1122h-1b_1-5> .

<http://rdf.glycoinfo.org/glycan/wurcs/2.0/Monosaccharide/axxxxh-1x_1-%3F_2%2ANCC%2F3%3DO>
        a       <http://www.glycoinfo.org/glyco/owl/wurcs#Monosaccharide> ;
        <http://www.glycoinfo.org/glyco/owl/wurcs#subsumes>
                <http://rdf.glycoinfo.org/glycan/wurcs/2.0/Monosaccharide/a2122h-1b_1-5_2%2ANCC%2F3%3DO> .

<http://rdf.glycoinfo.org/glycan/wurcs/2.0/MOD/2%2ANCC%2F3%3DO>
        a       <http://www.glycoinfo.org/glyco/owl/wurcs#MOD> ;
        <http://www.glycoinfo.org/glyco/owl/wurcs#has_LIPS>
                <http://rdf.glycoinfo.org/glycan/wurcs/2.0/LIPS/2> ;
        <http://www.glycoinfo.org/glyco/owl/wurcs#has_MAP>
                "*NCC/3=O"^^<http://www.w3.org/2001/XMLSchema#string> .

<http://rdf.glycoinfo.org/glycan/wurcs/2.0/Monosaccharide/axxxxh-1x_1-%3F>
        a       <http://www.glycoinfo.org/glyco/owl/wurcs#Monosaccharide> ;
        <http://www.glycoinfo.org/glyco/owl/wurcs#subsumes>
                <http://rdf.glycoinfo.org/glycan/wurcs/2.0/Monosaccharide/a1122h-1b_1-5> , <http://rdf.glycoinfo.org/glycan/wurcs/2.0/Monosaccharide/a1122h-1a_1-5> .

<http://rdf.glycoinfo.org/glycan/wurcs/2.0/Monosaccharide/a3344h-1b_1-%3F>
        a       <http://www.glycoinfo.org/glyco/owl/wurcs#Monosaccharide> ;
        <http://www.glycoinfo.org/glyco/owl/wurcs#subsumes>
                <http://rdf.glycoinfo.org/glycan/wurcs/2.0/Monosaccharide/a1122h-1b_1-5> .

<http://rdf.glycoinfo.org/glycan/wurcs/2.0/Monosaccharide/axxxxh-1a_1-5>
        a       <http://www.glycoinfo.org/glyco/owl/wurcs#Monosaccharide> ;
        <http://www.glycoinfo.org/glyco/owl/wurcs#subsumes>
                <http://rdf.glycoinfo.org/glycan/wurcs/2.0/Monosaccharide/a1122h-1a_1-5> .

<http://rdf.glycoinfo.org/glycan/wurcs/2.0/Basetype/u1122h>
        a       <http://www.glycoinfo.org/glyco/owl/wurcs#basetype> ;
        <http://www.glycoinfo.org/glyco/owl/wurcs#basetype>
                "u1122h"^^<http://www.w3.org/2001/XMLSchema#string> .

<http://rdf.glycoinfo.org/glycan/wurcs/2.0/Monosaccharide/axxxxh-1x_1-5>
        a       <http://www.glycoinfo.org/glyco/owl/wurcs#Monosaccharide> ;
        <http://www.glycoinfo.org/glyco/owl/wurcs#subsumes>
                <http://rdf.glycoinfo.org/glycan/wurcs/2.0/Monosaccharide/a1122h-1b_1-5> , <http://rdf.glycoinfo.org/glycan/wurcs/2.0/Monosaccharide/a1122h-1a_1-5> .

<http://rdf.glycoinfo.org/glycan/wurcs/2.0/Monosaccharide/a4344h-1b_1-%3F_2%2ANCC%2F3%3DO>
        a       <http://www.glycoinfo.org/glyco/owl/wurcs#Monosaccharide> ;
        <http://www.glycoinfo.org/glyco/owl/wurcs#subsumes>
                <http://rdf.glycoinfo.org/glycan/wurcs/2.0/Monosaccharide/a2122h-1b_1-5_2%2ANCC%2F3%3DO> .

<http://rdf.glycoinfo.org/glycan/wurcs/2.0/Monosaccharide/axxxxh-1b_1-5>
        a       <http://www.glycoinfo.org/glyco/owl/wurcs#Monosaccharide> ;
        <http://www.glycoinfo.org/glyco/owl/wurcs#subsumes>
                <http://rdf.glycoinfo.org/glycan/wurcs/2.0/Monosaccharide/a1122h-1b_1-5> .

<http://rdf.glycoinfo.org/glycan/wurcs/2.0/Monosaccharide/axxxxh-1b_1-%3F>
        a       <http://www.glycoinfo.org/glyco/owl/wurcs#Monosaccharide> ;
        <http://www.glycoinfo.org/glyco/owl/wurcs#subsumes>
                <http://rdf.glycoinfo.org/glycan/wurcs/2.0/Monosaccharide/a1122h-1b_1-5> .

<http://rdf.glycoinfo.org/glycan/wurcs/2.0/Monosaccharide/axxxxh-1x_1-5_2%2ANCC%2F3%3DO>
        a       <http://www.glycoinfo.org/glyco/owl/wurcs#Monosaccharide> ;
        <http://www.glycoinfo.org/glyco/owl/wurcs#subsumes>
                <http://rdf.glycoinfo.org/glycan/wurcs/2.0/Monosaccharide/a2122h-1b_1-5_2%2ANCC%2F3%3DO> .

<http://rdf.glycoinfo.org/glycan/wurcs/2.0/Monosaccharide/axxxxh-1b_1-5_2%2ANCC%2F3%3DO>
        a       <http://www.glycoinfo.org/glyco/owl/wurcs#Monosaccharide> ;
        <http://www.glycoinfo.org/glyco/owl/wurcs#subsumes>
                <http://rdf.glycoinfo.org/glycan/wurcs/2.0/Monosaccharide/a2122h-1b_1-5_2%2ANCC%2F3%3DO> .

<http://rdf.glycoinfo.org/glycan/wurcs/2.0/Monosaccharide/a2122h-1b_1-%3F_2%2ANCC%2F3%3DO>
        a       <http://www.glycoinfo.org/glyco/owl/wurcs#Monosaccharide> ;
        <http://www.glycoinfo.org/glyco/owl/wurcs#subsumes>
                <http://rdf.glycoinfo.org/glycan/wurcs/2.0/Monosaccharide/a2122h-1b_1-5_2%2ANCC%2F3%3DO> .

<http://rdf.glycoinfo.org/glycan/wurcs/2.0/Monosaccharide/a4344h-1x_1-5_2%2ANCC%2F3%3DO>
        a       <http://www.glycoinfo.org/glyco/owl/wurcs#Monosaccharide> ;
        <http://www.glycoinfo.org/glyco/owl/wurcs#subsumes>
                <http://rdf.glycoinfo.org/glycan/wurcs/2.0/Monosaccharide/a2122h-1b_1-5_2%2ANCC%2F3%3DO> .

<http://rdf.glycoinfo.org/glycan/wurcs/2.0/Monosaccharide/a1122h-1a_1-%3F>
        a       <http://www.glycoinfo.org/glyco/owl/wurcs#Monosaccharide> ;
        <http://www.glycoinfo.org/glyco/owl/wurcs#subsumes>
                <http://rdf.glycoinfo.org/glycan/wurcs/2.0/Monosaccharide/a1122h-1a_1-5> .

<http://rdf.glycoinfo.org/glycan/wurcs/2.0/Monosaccharide/uxxxxh_2%2ANCC%2F3%3DO>
        a       <http://www.glycoinfo.org/glyco/owl/wurcs#Monosaccharide> ;
        <http://www.glycoinfo.org/glyco/owl/wurcs#subsumes>
                <http://rdf.glycoinfo.org/glycan/wurcs/2.0/Monosaccharide/a2122h-1b_1-5_2%2ANCC%2F3%3DO> .

<http://rdf.glycoinfo.org/glycan/wurcs/2.0/LIP/2>
        a       <http://www.glycoinfo.org/glyco/owl/wurcs#LIP> ;
        <http://www.glycoinfo.org/glyco/owl/wurcs#has_SC_position>
                2 .

<http://rdf.glycoinfo.org/glycan/wurcs/2.0/Basetype/u2122h_2%2ANCC%2F3%3DO>
        a       <http://www.glycoinfo.org/glyco/owl/wurcs#basetype> ;
        <http://www.glycoinfo.org/glyco/owl/wurcs#basetype>
                "u2122h_2*NCC/3=O"^^<http://www.w3.org/2001/XMLSchema#string> .

<http://rdf.glycoinfo.org/glycan/wurcs/2.0/LIPS/2>
        a       <http://www.glycoinfo.org/glyco/owl/wurcs#LIPS> ;
        <http://www.glycoinfo.org/glyco/owl/wurcs#has_LIP>
                <http://rdf.glycoinfo.org/glycan/wurcs/2.0/LIP/2> ;
        <http://www.glycoinfo.org/glyco/owl/wurcs#is_fuzzy>
                false .

<http://rdf.glycoinfo.org/glycan/wurcs/2.0/Monosaccharide/a1122h-1x_1-%3F>
        a       <http://www.glycoinfo.org/glyco/owl/wurcs#Monosaccharide> ;
        <http://www.glycoinfo.org/glyco/owl/wurcs#subsumes>
                <http://rdf.glycoinfo.org/glycan/wurcs/2.0/Monosaccharide/a1122h-1b_1-5> , <http://rdf.glycoinfo.org/glycan/wurcs/2.0/Monosaccharide/a1122h-1a_1-5> .

<http://rdf.glycoinfo.org/glycan/wurcs/2.0/LIP/1>
        a       <http://www.glycoinfo.org/glyco/owl/wurcs#LIP> ;
        <http://www.glycoinfo.org/glyco/owl/wurcs#has_SC_position>
                1 .

<http://rdf.glycoinfo.org/glycan/wurcs/2.0/Monosaccharide/u3344h>
        a       <http://www.glycoinfo.org/glyco/owl/wurcs#Monosaccharide> ;
        <http://www.glycoinfo.org/glyco/owl/wurcs#subsumes>
                <http://rdf.glycoinfo.org/glycan/wurcs/2.0/Monosaccharide/a1122h-1b_1-5> , <http://rdf.glycoinfo.org/glycan/wurcs/2.0/Monosaccharide/a1122h-1a_1-5> .

<http://rdf.glycoinfo.org/glycan/wurcs/2.0/Monosaccharide/a4344h-1x_1-%3F_2%2ANCC%2F3%3DO>
        a       <http://www.glycoinfo.org/glyco/owl/wurcs#Monosaccharide> ;
        <http://www.glycoinfo.org/glyco/owl/wurcs#subsumes>
                <http://rdf.glycoinfo.org/glycan/wurcs/2.0/Monosaccharide/a2122h-1b_1-5_2%2ANCC%2F3%3DO> .

<http://rdf.glycoinfo.org/glycan/wurcs/2.0/Monosaccharide/a4344h-1b_1-5_2%2ANCC%2F3%3DO>
        a       <http://www.glycoinfo.org/glyco/owl/wurcs#Monosaccharide> ;
        <http://www.glycoinfo.org/glyco/owl/wurcs#subsumes>
                <http://rdf.glycoinfo.org/glycan/wurcs/2.0/Monosaccharide/a2122h-1b_1-5_2%2ANCC%2F3%3DO> .

<http://rdf.glycoinfo.org/glycan/wurcs/2.0/Monosaccharide/u1122h>
        a       <http://www.glycoinfo.org/glyco/owl/wurcs#Monosaccharide> ;
        <http://www.glycoinfo.org/glyco/owl/wurcs#subsumes>
                <http://rdf.glycoinfo.org/glycan/wurcs/2.0/Monosaccharide/a1122h-1b_1-5> , <http://rdf.glycoinfo.org/glycan/wurcs/2.0/Monosaccharide/a1122h-1a_1-5> .

<http://rdf.glycoinfo.org/glycan/wurcs/2.0/Monosaccharide/a2122h-1x_1-5_2%2ANCC%2F3%3DO>
        a       <http://www.glycoinfo.org/glyco/owl/wurcs#Monosaccharide> ;
        <http://www.glycoinfo.org/glyco/owl/wurcs#subsumes>
                <http://rdf.glycoinfo.org/glycan/wurcs/2.0/Monosaccharide/a2122h-1b_1-5_2%2ANCC%2F3%3DO> .

<http://rdf.glycoinfo.org/glycan/wurcs/2.0/Monosaccharide/a1122h-1a_1-5>
        a       <http://www.glycoinfo.org/glyco/owl/wurcs#Monosaccharide> ;
        <http://www.glycoinfo.org/glyco/owl/wurcs#has_MOD>
                <http://rdf.glycoinfo.org/glycan/wurcs/2.0/MOD/1-5> ;
        <http://www.glycoinfo.org/glyco/owl/wurcs#has_SC>
                <http://rdf.glycoinfo.org/glycan/wurcs/2.0/SkeletonCode/a1122h> ;
        <http://www.glycoinfo.org/glyco/owl/wurcs#has_anomeric_position>
                1 ;
        <http://www.glycoinfo.org/glyco/owl/wurcs#has_anomeric_symbol>
                "a"^^<http://www.w3.org/2001/XMLSchema#string> ;
        <http://www.glycoinfo.org/glyco/owl/wurcs#has_basetype>
                <http://rdf.glycoinfo.org/glycan/wurcs/2.0/Basetype/u1122h> ;
        <http://www.glycoinfo.org/glyco/owl/wurcs#has_ring>
                <http://rdf.glycoinfo.org/glycan/wurcs/2.0/MOD/1-5> ;
        <http://www.glycoinfo.org/glyco/owl/wurcs#subsumes>
                <http://rdf.glycoinfo.org/glycan/wurcs/2.0/Monosaccharide/a1122h-1a_1-5> .

<http://rdf.glycoinfo.org/glycan/wurcs/2.0/LIPS/1>
        a       <http://www.glycoinfo.org/glyco/owl/wurcs#LIPS> ;
        <http://www.glycoinfo.org/glyco/owl/wurcs#has_LIP>
                <http://rdf.glycoinfo.org/glycan/wurcs/2.0/LIP/1> ;
        <http://www.glycoinfo.org/glyco/owl/wurcs#is_fuzzy>
                false .

<http://rdf.glycoinfo.org/glycan/wurcs/2.0/Monosaccharide/a1122h-1b_1-%3F>
        a       <http://www.glycoinfo.org/glyco/owl/wurcs#Monosaccharide> ;
        <http://www.glycoinfo.org/glyco/owl/wurcs#subsumes>
                <http://rdf.glycoinfo.org/glycan/wurcs/2.0/Monosaccharide/a1122h-1b_1-5> .

<http://rdf.glycoinfo.org/glycan/wurcs/2.0/Monosaccharide/uxxxxh>
        a       <http://www.glycoinfo.org/glyco/owl/wurcs#Monosaccharide> ;
        <http://www.glycoinfo.org/glyco/owl/wurcs#subsumes>
                <http://rdf.glycoinfo.org/glycan/wurcs/2.0/Monosaccharide/a1122h-1b_1-5> , <http://rdf.glycoinfo.org/glycan/wurcs/2.0/Monosaccharide/a1122h-1a_1-5> .

<http://rdf.glycoinfo.org/glycan/wurcs/2.0/Monosaccharide/a2122h-1x_1-%3F_2%2ANCC%2F3%3DO>
        a       <http://www.glycoinfo.org/glyco/owl/wurcs#Monosaccharide> ;
        <http://www.glycoinfo.org/glyco/owl/wurcs#subsumes>
                <http://rdf.glycoinfo.org/glycan/wurcs/2.0/Monosaccharide/a2122h-1b_1-5_2%2ANCC%2F3%3DO> .

<http://rdf.glycoinfo.org/glycan/wurcs/2.0/Monosaccharide/a1122h-1x_1-5>
        a       <http://www.glycoinfo.org/glyco/owl/wurcs#Monosaccharide> ;
        <http://www.glycoinfo.org/glyco/owl/wurcs#subsumes>
                <http://rdf.glycoinfo.org/glycan/wurcs/2.0/Monosaccharide/a1122h-1b_1-5> , <http://rdf.glycoinfo.org/glycan/wurcs/2.0/Monosaccharide/a1122h-1a_1-5> .

<http://rdf.glycoinfo.org/glycan/wurcs/2.0/Monosaccharide/a1122h-1b_1-5>
        a       <http://www.glycoinfo.org/glyco/owl/wurcs#Monosaccharide> ;
        <http://www.glycoinfo.org/glyco/owl/wurcs#has_MOD>
                <http://rdf.glycoinfo.org/glycan/wurcs/2.0/MOD/1-5> ;
        <http://www.glycoinfo.org/glyco/owl/wurcs#has_SC>
                <http://rdf.glycoinfo.org/glycan/wurcs/2.0/SkeletonCode/a1122h> ;
        <http://www.glycoinfo.org/glyco/owl/wurcs#has_anomeric_position>
                1 ;
        <http://www.glycoinfo.org/glyco/owl/wurcs#has_anomeric_symbol>
                "b"^^<http://www.w3.org/2001/XMLSchema#string> ;
        <http://www.glycoinfo.org/glyco/owl/wurcs#has_basetype>
                <http://rdf.glycoinfo.org/glycan/wurcs/2.0/Basetype/u1122h> ;
        <http://www.glycoinfo.org/glyco/owl/wurcs#has_ring>
                <http://rdf.glycoinfo.org/glycan/wurcs/2.0/MOD/1-5> ;
        <http://www.glycoinfo.org/glyco/owl/wurcs#subsumes>
                <http://rdf.glycoinfo.org/glycan/wurcs/2.0/Monosaccharide/a1122h-1b_1-5> .

<http://rdf.glycoinfo.org/glycan/wurcs/2.0/Monosaccharide/u4344h_2%2ANCC%2F3%3DO>
        a       <http://www.glycoinfo.org/glyco/owl/wurcs#Monosaccharide> ;
        <http://www.glycoinfo.org/glyco/owl/wurcs#subsumes>
                <http://rdf.glycoinfo.org/glycan/wurcs/2.0/Monosaccharide/a2122h-1b_1-5_2%2ANCC%2F3%3DO> .

<http://rdf.glycoinfo.org/glycan/wurcs/2.0/Monosaccharide/a3344h-1a_1-%3F>
        a       <http://www.glycoinfo.org/glyco/owl/wurcs#Monosaccharide> ;
        <http://www.glycoinfo.org/glyco/owl/wurcs#subsumes>
                <http://rdf.glycoinfo.org/glycan/wurcs/2.0/Monosaccharide/a1122h-1a_1-5> .
```

#### RDF Schema

* WURCS-RDF-glycan

![WURCS-RDF-glycan](WURCS-RDF-glycan.svg)


* WURCS-RDF-monosaccharide

![WURCS-RDF-MS](WURCS-RDF-MS.svg)




## WURCS to WURCS-SPARQL

* 2021-11-10 **N-linked GlcNAc2Man3 core**



  * https://glyconavi.org/Tools/tool/cosmos.php -> WURCS TO SPARQL

  `WURCS=2.0/3,5,4/[a2122h-1b_1-5_2*NCC/3=O][a1122h-1b_1-5][a1122h-1a_1-5]/1-1-2-3-3/a4-b1_b4-c1_c3-d1_c6-e1`
   
   ![G22768VO](G22768VO.png)
   
   https://glytoucan.org/Structures/Glycans/G22768VO

   



### change graph name

* glycan graph
```
http://rdf.glycosmos.org/glycans/wurcsrdf
 ↓
http://rdf.glycoinfo.org/wurcs-seq-rdf-0.3-20190905.ttl
```

* Monosaccharide graph
```
http://rdf.glycosmos.org/glycans/wurcsrdf
 ↓
http://rdf.glycoinfo.org/wurcs-ms-rdf-0.2-20190905.ttl
```


### WURCS-SPARQL of `WURCS=2.0/3,5,4/[a2122h-1b_1-5_2*NCC/3=O][a1122h-1b_1-5][a1122h-1a_1-5]/1-1-2-3-3/a4-b1_b4-c1_c3-d1_c6-e1`

```
# ******************************************************
# SPARQL Generator for Glycan Search:
# Last update: 2015/12/14
# Query Structure:
# WURCS=2.0/3,5,4/[a2122h-1b_1-5_2*NCC/3=O][a1122h-1b_1-5][a1122h-1a_1-5]/1-1-2-3-3/a4-b1_b4-c1_c3-d1_c6-e1
# ******************************************************

DEFINE sql:select-option "order"
PREFIX glycan: <http://purl.jp/bio/12/glyco/glycan#>
PREFIX wurcs: <http://www.glycoinfo.org/glyco/owl/wurcs#>
PREFIX xsd: <http://www.w3.org/2001/XMLSchema#>
SELECT DISTINCT ?glycan (COUNT (?glycan) AS ?gcount) (STR (?wurcs) AS ?WURCS) 
FROM <http://rdf.glycoinfo.org/wurcs-seq-rdf-0.3-20190905.ttl>
FROM NAMED <http://rdf.glycoinfo.org/wurcs-ms-rdf-0.2-20190905.ttl>
WHERE {
  ?glycan glycan:has_glycosequence ?gseq .
  ?gseq wurcs:count_RES ?rescount .
  ?gseq wurcs:count_uniqueRES ?urescount .
  ?gseq wurcs:count_LIN ?lincount .
  ?gseq glycan:has_sequence ?wurcs .

  # GLIN1

  ## GRES1
  ?gseq wurcs:has_GRES ?GRES1 .
  ?GRES1 wurcs:is_monosaccharide ?MS1 .
  GRAPH <http://rdf.glycoinfo.org/wurcs-ms-rdf-0.2-20190905.ttl> {
    <http://rdf.glycoinfo.org/glycan/wurcs/2.0/Monosaccharide/a2122h-1b_1-5_2%2ANCC%2F3%3DO> wurcs:subsumes ?MS1 .
  }
  ?GRES1 wurcs:is_acceptor_of ?GLIN1 .

  ?GLIN1 wurcs:has_donor_position 1 .
  ?GLIN1 wurcs:has_acceptor_position 4 .

  ## GRES2
  ?gseq wurcs:has_GRES ?GRES2 .
  ?GRES2 wurcs:is_monosaccharide ?MS1 .
  FILTER ( ?GRES2 != ?GRES1 )

  ?GRES2 wurcs:is_donor_of ?GLIN1 .


  # GLIN2
  ?GRES2 wurcs:is_acceptor_of ?GLIN2 .

  ?GLIN2 wurcs:has_donor_position 1 .
  ?GLIN2 wurcs:has_acceptor_position 4 .

  ## GRES3
  ?gseq wurcs:has_GRES ?GRES3 .
  ?GRES3 wurcs:is_monosaccharide ?MS2 .
  GRAPH <http://rdf.glycoinfo.org/wurcs-ms-rdf-0.2-20190905.ttl> {
    <http://rdf.glycoinfo.org/glycan/wurcs/2.0/Monosaccharide/a1122h-1b_1-5> wurcs:subsumes ?MS2 .
  }
  FILTER ( ?GRES3 != ?GRES1 && ?GRES3 != ?GRES2 )

  ?GRES3 wurcs:is_donor_of ?GLIN2 .


  # GLIN3
  ?GRES3 wurcs:is_acceptor_of ?GLIN3 .

  ?GLIN3 wurcs:has_donor_position 1 .
  ?GLIN3 wurcs:has_acceptor_position 3 .

  ## GRES4
  ?gseq wurcs:has_GRES ?GRES4 .
  ?GRES4 wurcs:is_monosaccharide ?MS3 .
  GRAPH <http://rdf.glycoinfo.org/wurcs-ms-rdf-0.2-20190905.ttl> {
    <http://rdf.glycoinfo.org/glycan/wurcs/2.0/Monosaccharide/a1122h-1a_1-5> wurcs:subsumes ?MS3 .
  }
  FILTER ( ?GRES4 != ?GRES1 && ?GRES4 != ?GRES2 && ?GRES4 != ?GRES3 )

  ?GRES4 wurcs:is_donor_of ?GLIN3 .


  # GLIN4
  ?GRES3 wurcs:is_acceptor_of ?GLIN4 .

  ?GLIN4 wurcs:has_donor_position 1 .
  ?GLIN4 wurcs:has_acceptor_position 6 .

  ## GRES5
  ?gseq wurcs:has_GRES ?GRES5 .
  ?GRES5 wurcs:is_monosaccharide ?MS3 .
  FILTER ( ?GRES5 != ?GRES1 && ?GRES5 != ?GRES2 && ?GRES5 != ?GRES3 && ?GRES5 != ?GRES4 )

  ?GRES5 wurcs:is_donor_of ?GLIN4 .


  # MAP

  ?GLIN1 wurcs:has_MAP ""^^xsd:string .
  ?GLIN2 wurcs:has_MAP ""^^xsd:string .
  ?GLIN3 wurcs:has_MAP ""^^xsd:string .
  ?GLIN4 wurcs:has_MAP ""^^xsd:string .
}
ORDER BY ?rescount ?urescount ?lincount ?glycan desc (?gcount)
```



### SPARQL Endpoint

* https://sparql.glyconavi.org/sparql

